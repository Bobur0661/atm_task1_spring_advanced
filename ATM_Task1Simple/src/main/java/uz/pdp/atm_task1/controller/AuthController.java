package uz.pdp.atm_task1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.atm_task1.payload.ApiResponse;
import uz.pdp.atm_task1.payload.LoginDto;
import uz.pdp.atm_task1.payload.RegisterDto;
import uz.pdp.atm_task1.service.AuthService;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/auth")
public class AuthController {


    @Autowired
    AuthService authService;

    /**
     * Birinchi yangi hali ro'yxatdan o'tmagan user, ro'yxatdan o'tishi uchun yozilgan method!
     *
     * @param dto RegisterDto
     * @return ApiResponse
     */
    @PostMapping("/register")
    public HttpEntity<?> register(@RequestBody @Valid RegisterDto dto) {
        ApiResponse apiResponse = authService.registerUser(dto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }


    @PostMapping("/login")
    public HttpEntity<?> login(@RequestBody @Valid LoginDto dto) {
        ApiResponse apiResponse = authService.login(dto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }


}
