package uz.pdp.atm_task1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.atm_task1.payload.ApiResponse;
import uz.pdp.atm_task1.payload.CardDto;
import uz.pdp.atm_task1.service.CardService;

import java.util.UUID;

@RestController
@RequestMapping("/api/card")
public class CardController {

    @Autowired
    CardService cardService;

    // Card crud hali oxiriga yetmagan

    @PostMapping
    public HttpEntity<?> addTheCard(@RequestBody CardDto dto) {
        ApiResponse apiResponse = cardService.addNewCard(dto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @GetMapping
    public HttpEntity<?> getAllCards() {
        ApiResponse apiResponse = cardService.getAllCards();
        return ResponseEntity.ok(apiResponse);
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getCardById(@PathVariable UUID id) {
        ApiResponse apiResponse = cardService.getCardById(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @PutMapping("/{id}")
    public HttpEntity<?> updateCard(@PathVariable UUID id, @RequestBody CardDto dto) {
        ApiResponse apiResponse = cardService.updateCard(id, dto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 202 : 409).body(apiResponse);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteCard(@PathVariable UUID id) {
        ApiResponse apiResponse = cardService.deleteCardById(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 204 : 409).body(apiResponse);
    }

}
