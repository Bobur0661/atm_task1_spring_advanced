package uz.pdp.atm_task1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.atm_task1.entity.Commission;
import uz.pdp.atm_task1.payload.ApiResponse;
import uz.pdp.atm_task1.service.CommissionService;

@RestController
@RequestMapping("/api/commission")
public class CommissionController {

    @Autowired
    CommissionService commissionService;


    @GetMapping
    public HttpEntity<?> getCommission() {
        ApiResponse apiResponse = commissionService.getCommission();
        return ResponseEntity.ok(apiResponse);
    }


    @GetMapping("/{id}")
    public HttpEntity<?> getCommissionByID(@PathVariable Long id) {
        ApiResponse apiResponse = commissionService.getCommissionById(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }


    @PostMapping("/add")
    public HttpEntity<?> addCommission(@RequestBody Commission commission) {
        ApiResponse apiResponse = commissionService.addCommission(commission);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }


    @PatchMapping("/{id}")
    public HttpEntity<?> editCommission(@PathVariable Long id, @RequestBody Commission commission) {
        ApiResponse apiResponse = commissionService.editCommission(id, commission);
        return ResponseEntity.status(apiResponse.isSuccess() ? 202 : 409).body(apiResponse);
    }


    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteCommission(@PathVariable Long id) {
        ApiResponse apiResponse = commissionService.deleteCommission(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 204 : 409).body(apiResponse);
    }


}
