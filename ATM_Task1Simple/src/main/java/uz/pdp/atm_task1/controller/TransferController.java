package uz.pdp.atm_task1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.atm_task1.payload.ApiResponse;
import uz.pdp.atm_task1.payload.TransDto;
import uz.pdp.atm_task1.service.TransferService;

@RestController
@RequestMapping("/api/transfer")
public class TransferController {


    @Autowired
    TransferService transferService;


    @PostMapping
    HttpEntity<?> transferMoneyFromOneCardToAnother(@RequestBody TransDto dto) {
        ApiResponse apiResponse = transferService.transferMoneyFromCardToCard(dto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }


}
