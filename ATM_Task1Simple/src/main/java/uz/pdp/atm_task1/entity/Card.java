package uz.pdp.atm_task1.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Card {

    // ikkita karta qo'shaman va

    @Id
    @GeneratedValue
    private UUID id;

    @Column(nullable = false)
    private String name;

    @Column(unique = true, nullable = false)
    private String number;

    private double balance;

    @Column(nullable = false)
    private LocalDate expireDate;

    @ManyToOne
    private User user;

    private boolean active = true;

    public Card(String name, String number, double balance, User user) {
        this.name = name;
        this.number = number;
        this.balance = balance;
        this.user = user;
    }
}
