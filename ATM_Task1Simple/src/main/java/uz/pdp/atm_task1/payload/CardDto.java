package uz.pdp.atm_task1.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CardDto {

    // bitta pul o'tkazish uchun dto ochib usha dto ga yo'l yo'zishim kerak.

    @NotNull(message = "Cannot be blank")
    private String name;

    @NotNull(message = "Cannot be blank")
    private String number;

    @NotNull(message = "Cannot be blank")
    private double balance;

    @NotNull(message = "Cannot be blank")
    private LocalDate expireDate;

    @NotNull(message = "Cannot be blank")
    private UUID userId;


}
