package uz.pdp.atm_task1.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginDto {

    @NotNull(message = "Cannot be blank!")
    private String username;

    @NotNull(message = "Password cannot be blank!")
    private String password;

}
