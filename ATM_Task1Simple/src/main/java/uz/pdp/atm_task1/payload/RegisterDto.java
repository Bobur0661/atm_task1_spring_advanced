package uz.pdp.atm_task1.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterDto {


    @Size(min = 2, max = 50)
    @NotNull(message = "Name cannot be blank!")
    private String name;

    @Size(min = 2, max = 50)
    @NotNull(message = "Surname cannot be blank!")
    private String surname;

    @NotNull(message = "Username cannot be blank!")
    private String username;

    @NotNull(message = "Password cannot be blank!")
    private String password;

}
