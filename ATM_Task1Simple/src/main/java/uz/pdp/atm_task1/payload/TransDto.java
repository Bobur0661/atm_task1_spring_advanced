package uz.pdp.atm_task1.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransDto {


    private UUID fromCard;
    private UUID toCard;
    private double amount;
    private Long commissionId;

}
