package uz.pdp.atm_task1.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.atm_task1.entity.Card;

import java.util.UUID;

public interface CardRepo extends JpaRepository<Card, UUID> {

    boolean existsByNumber(String number);

    boolean existsByNumberAndIdNot(String number, UUID id);

}
