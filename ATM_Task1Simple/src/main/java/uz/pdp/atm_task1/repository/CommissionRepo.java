package uz.pdp.atm_task1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.atm_task1.entity.Commission;

public interface CommissionRepo extends JpaRepository<Commission, Long> {

    boolean existsByActiveAndPercent(boolean active, double percent);

}
