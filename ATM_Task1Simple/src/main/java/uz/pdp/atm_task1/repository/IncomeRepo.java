package uz.pdp.atm_task1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.atm_task1.entity.Income;

public interface IncomeRepo extends JpaRepository<Income, Long> {
}
