package uz.pdp.atm_task1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.atm_task1.entity.Income;
import uz.pdp.atm_task1.entity.Outcome;

public interface OutcomeRepo extends JpaRepository<Outcome, Long> {
}
