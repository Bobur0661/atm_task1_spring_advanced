package uz.pdp.atm_task1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.atm_task1.entity.User;

import java.util.Optional;
import java.util.UUID;

public interface UserRepo extends JpaRepository<User, UUID> {


    boolean existsByUsername(String username);

    Optional<User> findByUsername(String username);

}
