package uz.pdp.atm_task1.secret;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import uz.pdp.atm_task1.entity.User;
import uz.pdp.atm_task1.service.AuthService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtFilter extends OncePerRequestFilter {

    @Autowired
    JwtProvider jwtProvider;

    // agar JtwProvider class ni ichida getUserFromToken() shu methodni ichida userni username bo'yicha
//    olib keladigan JpaQuery yozmaganimizda AuthService dan loadByUsername methodini ishlatib user ni olib kelardik.
    @Autowired
    AuthService authService;


    // Front tokenni bizda header fda jo'natgan dn so'ng biz uni olvolib yana tekshiramiz.
    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {

        String token = request.getHeader("Authorization");
        if (token != null && token.startsWith("Bearer")) {
            token = token.substring(7);

            boolean validateToken = jwtProvider.validateToken(token);
            if (validateToken) {
                User user = jwtProvider.getUserFromToken(token);
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                        new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            }
        }
        filterChain.doFilter(request, response);
    }
}
