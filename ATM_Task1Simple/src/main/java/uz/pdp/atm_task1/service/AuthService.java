package uz.pdp.atm_task1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.pdp.atm_task1.entity.User;
import uz.pdp.atm_task1.payload.ApiResponse;
import uz.pdp.atm_task1.payload.LoginDto;
import uz.pdp.atm_task1.payload.RegisterDto;
import uz.pdp.atm_task1.repository.UserRepo;
import uz.pdp.atm_task1.secret.JwtProvider;

import java.util.Collections;

@Service
public class AuthService implements UserDetailsService {


    @Autowired
    UserRepo userRepo;

    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    AuthenticationManager authenticationManager;


    /**
     * Birinchi yangi hali ro'yxatdan o'tmagan user, ro'yxatdan o'tishi uchun yozilgan method!
     *
     * @param dto RegisterDto
     * @return ApiResponse
     */
    public ApiResponse registerUser(RegisterDto dto) {
        boolean existsByUsername = userRepo.existsByUsername(dto.getUsername());
        if (existsByUsername) {
            return new ApiResponse(false, "The username already exists! Please Enter another one.");
        }
        User user = new User(dto.getName(), dto.getSurname(),
                dto.getUsername(), passwordEncoder.encode(dto.getPassword()));
        userRepo.save(user);
        return new ApiResponse(true, "Registered Successfully.");
    }

    /**
     * User Login qilganda uni tekshiradigan va unga Token generatsita qilib beradi.
     * va o'sha tokenni biz front ga bervoramiz va front bizga tokenni header da jo'natadi.
     *
     * @param dto LoginDto
     * @return ApiResponse
     */
    public ApiResponse login(LoginDto dto) {
        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(dto.getUsername(), dto.getPassword()));
            User user = (User) authentication.getPrincipal();
//             JWT
            String token = jwtProvider.generateToken(user.getUsername());
            return new ApiResponse(true, "Success", token);
        } catch (BadCredentialsException e) {
            return new ApiResponse(false, "Password or username is wrong!");
        }
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepo.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username + "Not Found!"));
    }
}
