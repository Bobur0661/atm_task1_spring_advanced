package uz.pdp.atm_task1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.atm_task1.entity.Card;
import uz.pdp.atm_task1.entity.User;
import uz.pdp.atm_task1.payload.ApiResponse;
import uz.pdp.atm_task1.payload.CardDto;
import uz.pdp.atm_task1.repository.CardRepo;
import uz.pdp.atm_task1.repository.UserRepo;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class CardService {

    @Autowired
    CardRepo cardRepo;

    @Autowired
    UserRepo userRepo;


    public ApiResponse addNewCard(CardDto dto) {
        boolean existsByNumber = cardRepo.existsByNumber(dto.getNumber());
        if (existsByNumber) {
            return new ApiResponse(false, "The Card number already exists! Please Enter another one.");
        }

        Optional<User> optionalUser = userRepo.findById(dto.getUserId());
        if (optionalUser.isPresent()) {
            Card card = new Card(dto.getName(), dto.getNumber(), dto.getBalance(),
                    optionalUser.get());
            cardRepo.save(card);
            return new ApiResponse(true, "Saved!");
        }
        return new ApiResponse(false, "Card not Found!");
    }

    public ApiResponse getAllCards() {
        List<Card> all = cardRepo.findAll();
        return new ApiResponse(true, "Success", all);
    }

    public ApiResponse getCardById(UUID id) {
        Optional<Card> optionalCard = cardRepo.findById(id);
        return optionalCard.map(card -> new ApiResponse(true, "Success", card)).orElseGet(() -> new ApiResponse(false, "Card not Found!"));
    }

    public ApiResponse updateCard(UUID id, CardDto dto) {
        Optional<Card> optionalCard = cardRepo.findById(id);
        if (!optionalCard.isPresent()) {
            return new ApiResponse(false, "Card not Found!");
        }
        boolean existsByNumberAndIdNot = cardRepo.existsByNumberAndIdNot(dto.getNumber(), id);
        if (existsByNumberAndIdNot) {
            return new ApiResponse(false, "This card number already exists! Please Enter another one!");
        }

        Optional<User> optionalUser = userRepo.findById(dto.getUserId());
        if (!optionalUser.isPresent()) {
            return new ApiResponse(false, "User not Found!");
        }
        Card card = optionalCard.get();
        card.setBalance(dto.getBalance());
        card.setExpireDate(dto.getExpireDate());
        card.setName(dto.getName());
        card.setNumber(dto.getNumber());
        card.setUser(optionalUser.get());
        cardRepo.save(card);
        return new ApiResponse(true, "Updated!");
    }


    public ApiResponse deleteCardById(UUID id) {
        try {
            cardRepo.deleteById(id);
            return new ApiResponse(true, "Deleted!");
        } catch (Exception e) {
            return new ApiResponse(false, "Error on deleting!");
        }
    }
}
