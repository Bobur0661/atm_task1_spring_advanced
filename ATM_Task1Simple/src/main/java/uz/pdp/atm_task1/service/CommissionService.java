package uz.pdp.atm_task1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.atm_task1.entity.Commission;
import uz.pdp.atm_task1.payload.ApiResponse;
import uz.pdp.atm_task1.repository.CommissionRepo;

import java.util.List;
import java.util.Optional;

@Service
public class CommissionService {

    @Autowired
    CommissionRepo commissionRepo;

    /**
     * Get all Commissions
     *
     * @return ApiResponse
     */
    public ApiResponse getCommission() {
        List<Commission> allCommissions = commissionRepo.findAll();
        return new ApiResponse(true, "Success", allCommissions);
    }


    /**
     * Getting Commission By Id
     *
     * @param id Long
     * @return ApiResponse
     */
    public ApiResponse getCommissionById(Long id) {
        Optional<Commission> optionalCommission = commissionRepo.findById(id);
        return optionalCommission.map(commission -> new ApiResponse(true, "Success", commission))
                .orElseGet(() -> new ApiResponse(false, "Commission not Found!"));
    }


    public ApiResponse addCommission(Commission commission) {
        boolean existsByActiveAndPercent = commissionRepo.existsByActiveAndPercent(true, commission.getPercent());
        if (!existsByActiveAndPercent) {
            commissionRepo.save(commission);
            return new ApiResponse(true, "Saved");
        }
        return new ApiResponse(false, "The product already exists!");
    }


    public ApiResponse editCommission(Long id, Commission commission) {
        Optional<Commission> optionalCommission = commissionRepo.findById(id);
        if (optionalCommission.isPresent()) {
            Commission commission1 = optionalCommission.get();
            commission1.setPercent(commission.getPercent());
            commission1.setActive(commission.isActive());
            commissionRepo.save(commission1);
            return new ApiResponse(true, "Edited", commission1);
        }
        return new ApiResponse(false, "Commission not Found");
    }


    public ApiResponse deleteCommission(Long id) {
        try {
            commissionRepo.deleteById(id);
            return new ApiResponse(true, "Deleted!");
        } catch (Exception e) {
            return new ApiResponse(false, "Error on Deleting!");
        }
    }
}
