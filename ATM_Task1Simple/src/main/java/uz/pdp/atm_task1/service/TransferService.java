package uz.pdp.atm_task1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.atm_task1.entity.Card;
import uz.pdp.atm_task1.entity.Commission;
import uz.pdp.atm_task1.entity.Income;
import uz.pdp.atm_task1.entity.Outcome;
import uz.pdp.atm_task1.payload.ApiResponse;
import uz.pdp.atm_task1.payload.TransDto;
import uz.pdp.atm_task1.repository.CardRepo;
import uz.pdp.atm_task1.repository.CommissionRepo;
import uz.pdp.atm_task1.repository.IncomeRepo;
import uz.pdp.atm_task1.repository.OutcomeRepo;

import java.util.Optional;

@Service
public class TransferService {


    @Autowired
    CardRepo cardRepo;

    @Autowired
    CommissionRepo commissionRepo;

    @Autowired
    IncomeRepo incomeRepo;

    @Autowired
    OutcomeRepo outcomeRepo;


    public ApiResponse transferMoneyFromCardToCard(TransDto dto) {
        Optional<Card> fromCard = cardRepo.findById(dto.getFromCard());
        if (!fromCard.isPresent()) {
            return new ApiResponse(false, "The Output Card cannot be Found!");
        }

        Optional<Card> toCard = cardRepo.findById(dto.getToCard());
        if (!toCard.isPresent()) {
            return new ApiResponse(false, "The Input Card cannot be Found!");
        }

        Optional<Commission> optionalCommission = commissionRepo.findById(dto.getCommissionId());
        if (!optionalCommission.isPresent()) {
            return new ApiResponse(false, "Commission Not Found!");
        }

        Commission commission = optionalCommission.get();
        Card outputCard = fromCard.get();
        Card inputCard = toCard.get();

        if (dto.getAmount() > 0 &&
                outputCard.getBalance() >= (dto.getAmount() + (dto.getAmount() * commission.getPercent()) / 100)) {

            inputCard.setBalance(inputCard.getBalance()+dto.getAmount());
            outputCard.setBalance(
                    outputCard.getBalance() - (dto.getAmount() + (dto.getAmount() * commission.getPercent()) / 100));

            // log chiqim card ga yozish
            Outcome outcome = new Outcome();
            Income income = new Income();
            outcome.setFromCard(outputCard);
            outcome.setToCard(inputCard);
            outcome.setOutcome_amount((dto.getAmount() + (dto.getAmount() * commission.getPercent()) / 100));
            outcome.setCommission(commission);
            outcomeRepo.save(outcome);

            // log kirib card ga yizish
            income.setFromCard(outputCard);
            income.setToCard(inputCard);
            income.setIncome_amount(dto.getAmount());
            incomeRepo.save(income);

            cardRepo.save(inputCard);
            cardRepo.save(outputCard);
            return new ApiResponse(true, "Transfer completed!");
        } else {
            return new ApiResponse(false, "Not Enough money in Output Card!");
        }

    }
}
